const form = document.querySelector('#myForm');

const submitHandler = (e) => {
    e.preventDefault();
    form.querySelector('input[name="hiddenInput"]').remove();
    let new_form = new FormData(form);
    new_form.append('newField', 'customValue');
    fetch('/posturl/', {
        body: new_form,
        method: 'POST'
    });
    document.querySelector('.submit_text').innerHTML = 'Форма отправлена!'
};

form.addEventListener('submit', submitHandler);

