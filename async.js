const RESULT = {
  resolve: 'Promise fulfilled',
  reject: 'reject rejected',
};

const callPromise = () => new Promise((resolve, reject) => {
  const success = (Math.floor(Math.random() * 200) + 1) > 100;

  setTimeout(() => {
    if (success) {
      resolve(RESULT.resolve);
    } else {
      reject(new Error(RESULT.reject));
    }
  }, 900);
});

/*Задача 1. Написать функцию task1, которая вызывает callPromise() и в зависимости от результата завершения промиса делает следующее:
   - fulfilled: вывести в консоль результат выполнения промиса
   - rejected: вызвать алерт с текстом "Произошла ошибка". 
   
   Задачу 1 реализовать только с использованием Promise API */

const task1 = callPromise();

task1
  .then((result) => {
    console.log('Результат:', result);
  })
  .catch((err) => {
    alert('Произошла ошибка');
  })

/*Задача 2. Написать функцию task2, которая вызывает callPromise() и в зависимости от результата завершения промиса делает следующее:
- fulfilled: вывести в консоль результат выполнения промиса
- rejected: вызвать алерт с текстом "Произошла ошибка"

Задачу 2 реализовать с помощью конструкций async / await */

async function task2() {
  try {
    let result = await callPromise();
    console.log("Результат:", result);
  } catch (err) {
    alert('Произошла ошибка');
  }
}

task2();










